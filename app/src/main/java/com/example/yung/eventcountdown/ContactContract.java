//ContactContract
package com.example.yung.eventcountdown;

import android.provider.BaseColumns;

/**
 * Created by yung on 2017/8/20.
 */
public class ContactContract {
    private ContactContract() {
    }

    public static class ContactEntry implements BaseColumns {
        public static final String TABLE_NAME = "contact2";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_DOB = "dob";
    }
}
