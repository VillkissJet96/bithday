//Contact
package com.example.yung.eventcountdown;

/**
 * Created by yung on 2017/8/20.
 */

public class Contact implements java.io.Serializable {
    private long id;
    private String name;
    private String email;
    private String dob;

    public Contact(String name, String email, String dob) {
        this.id = 0;
        this.name = name;
        this.email = email;
        this.dob = dob;
    }

    public Contact(long id, String name, String email, String dob) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dob = dob;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getDob() {
        return dob;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
