//ListTodayActivity
package com.example.yung.eventcountdown;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {
    private Contact contact;
    public static final String EXTRA_ID = "com.example.yung.eventcountdown.ID";

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        NotificationTask task = new NotificationTask();
        Calendar calendar = Calendar.getInstance();
        Calendar nowCalendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        long millis = calendar.getTimeInMillis();
        long nowmillis = nowCalendar.getTimeInMillis();
        Timer timer = new Timer();
        if (millis - nowmillis > 0)
            timer.schedule(task, millis - nowmillis);
        else
            timer.schedule(task, nowmillis - millis);
    }

    class NotificationTask extends TimerTask {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        public void run() {
            showRandom();
        }
    }

    private String date;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void showRandom() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM");
        Calendar calendar = Calendar.getInstance();
        date = sdf.format(calendar.getTime());
        ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(getApplicationContext()));
        final String[] columns = {
                ContactContract.ContactEntry._ID,
                ContactContract.ContactEntry.COLUMN_NAME_NAME,
                ContactContract.ContactEntry.COLUMN_NAME_EMAIL,
                ContactContract.ContactEntry.COLUMN_NAME_DOB
        };
        String[] date_where = {"%" + date + "%"};
        Cursor cursor = dbq.query(columns, "dob like ? ", date_where, null, null, null);
        sendNotification(cursor);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void sendNotification(Cursor cursor) {
        Intent intent = new Intent(this, ListTodayActivity.class);
        intent.putExtra(EXTRA_ID, 5);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        int id = 1;
        String test = null;
        int count = cursor.getCount();
        if (count != 0) {
            if (cursor.moveToNext()) {
                test = cursor.getString(cursor.getColumnIndex(ContactContract.ContactEntry.COLUMN_NAME_NAME));
            }
            Notification.Builder nBuilder =
                    new Notification.Builder(this)
                            .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                            .setContentTitle("Today got " + count + " buddy(s) birthday!")
                            .setContentText("Click to show details. ")
                            .setContentIntent(resultPendingIntent);
            NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nManager.notify(id, nBuilder.build());
        } else {

        }
    }
}
