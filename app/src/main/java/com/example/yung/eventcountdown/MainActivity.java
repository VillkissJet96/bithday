//ListTodayActivity
package com.example.yung.eventcountdown;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = new Intent(this, MyIntentService.class);
        startService(intent);
    }

    public void viewAll(View view) {


        Intent intent = new Intent(getApplicationContext(), ListContactActivity.class);
        startActivity(intent);
    }

    public void viewToday(View view) {
        Intent intent = new Intent(getApplicationContext(), ListTodayActivity.class);
        startActivity(intent);
    }

    public void viewTmr(View view) {
        Intent intent = new Intent(getApplicationContext(), ListTmrActivity.class);
        startActivity(intent);
    }

    public void cloudBackUp(View view) {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new PostJsonTask(MainActivity.this).execute();
        } else {
            Toast toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.network_unavailable), Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
