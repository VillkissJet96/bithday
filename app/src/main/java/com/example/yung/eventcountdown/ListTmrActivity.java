//ListTmrActivity
package com.example.yung.eventcountdown;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ListTmrActivity extends AppCompatActivity {
    public static final String EXTRA_ID = "com.example.yung.eventcountdown.ID";
    private ListView contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivity(intent);
            }
        });
        contactList = (ListView) findViewById(R.id.contact_list);
        contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                               @Override
                                               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                   Cursor cursor = (Cursor) parent.getItemAtPosition(position);

                                                   Intent intent = new Intent(ListTmrActivity.this, ViewContactActivity.class);
                                                   intent.putExtra(EXTRA_ID, cursor.getLong(cursor.getColumnIndex(ContactContract.ContactEntry._ID)));
                                                   ListTmrActivity.this.startActivity(intent);
                                               }
                                           }
        );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ContactDbQueries dbq = new ContactDbQueries(new ContactDbHelper(getApplicationContext()));
        Calendar calendar = Calendar.getInstance();
        Calendar calendar_tmr = Calendar.getInstance();
        calendar_tmr.add(Calendar.DAY_OF_MONTH, 1);
        Calendar calendar_tmr2 = Calendar.getInstance();
        calendar_tmr2.add(Calendar.DAY_OF_MONTH, 2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM");
        String date = sdf.format(calendar.getTime());
        String date_tmr = sdf.format(calendar_tmr.getTime());
        String date_tmr2 = sdf.format(calendar_tmr2.getTime());
        String tmr = "%" + date_tmr + "%";
        String tmr2 = "%" + date_tmr2 + "%";
        String selection = "dob like ? OR dob like ?";
        String[] where = {
                tmr, tmr2
        };
        String[] columns = {
                ContactContract.ContactEntry._ID,
                ContactContract.ContactEntry.COLUMN_NAME_NAME,
                ContactContract.ContactEntry.COLUMN_NAME_EMAIL,
                ContactContract.ContactEntry.COLUMN_NAME_DOB
        };
        Cursor cursor = dbq.query(columns, selection, where, null, null, ContactContract.ContactEntry.COLUMN_NAME_DOB + " ASC");
        ContactCursorAdapter adapter = new ContactCursorAdapter(this, cursor, 0);
        contactList.setAdapter(adapter);
    }

}
