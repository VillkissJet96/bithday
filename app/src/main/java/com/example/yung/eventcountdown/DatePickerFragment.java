//DatePickerFragment
package com.example.yung.eventcountdown;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by yung on 2017/8/20.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    static Calendar calendar = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use current date
        int year, month, day;
        DatePicker dp;
        DatePickerDialog dpd;
        if (calendar == null) {
            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            dpd = new DatePickerDialog(getActivity(), this, year, month, day);
            dp = dpd.getDatePicker();
            calendar.add(Calendar.DAY_OF_MONTH, 0);
            dp.setMaxDate(calendar.getTimeInMillis());
        } else {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            dpd = new DatePickerDialog(getActivity(), this, year, month, day);
            dp = dpd.getDatePicker();
            calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            dp.setMaxDate(calendar.getTimeInMillis());
        }
        return dpd;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        //calendar.add(Calendar.DATE,1);
        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMMM, yyyy");
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);   // In Java, months are counted from 0 to 11.
        calendar.set(Calendar.DAY_OF_MONTH, day);
        String date = sdf.format(calendar.getTime());
        EditText etDate = (EditText) getActivity().findViewById(R.id.dob);
        //etDate.setText(year + "-" + String.format("%02d", month+1) + "-" + String.format("%02d", day));
        etDate.setText(date);


    }
}
